<?php

class B
{
    /* @var PDO */
    protected $dbConn;
    
    /* @var integer */
    protected $id;
    
    
    public function __construct(PDO $conn)
    {
        $this->dbConn = $conn;
    }
    
    public function getId()
    {
    	return $this->id;
    }
    
    public function setId($id)
    {
    	$this->id = $id;
    	return $this;
    }
    
    public function processInput($name)
    {
        $value = $_REQUEST[$name];
        $value = htmlspecialchars($value);
        
        $dbValue = $this->dbConn->quote($value);
        $this->saveField('input', $dbValue);
        
        return $value;
    }
    
    public function uploadFile($name)
    {
        if (!$_FILES[$name]['name'] && !$_FILES[$name]['size']) {
            return [];
        }
        
        if ($_FILES[$name]['error'] != 0) {
            throw new Exception('Error while downloading file');
        }
        
        $file = file_get_content($_FILES[$name]['tmp_name']);
        $this->saveField('file', $file);
        
        return [
        	'id'	=> $this->getId(),
            'name'  => $_FILES[$name]['name'],
        ];
    }
    
    protected function saveField($name, $val)
    {
        if ($this->getId()) {
        	$this->dbConn->exec("UPDATE `files` SET `". $name ."` = '" . $val . "' WHERE id = " . $this->getId());
        } else {
        	$this->dbConn->exec("INSERT `files` (`". $name ."`) VALUES ('" . $val . "')");
        	$this->setId($this->dbConn->lastInsertId());
        }
        
        return $this;
    }
    
}
