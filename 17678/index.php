<?php

include_once 'B.php';

const DB_DSN		= 'mysql:dbname=test;host=127.0.0.1';
const DB_USER		= 'admin';
const DB_PASSWORD	= '';


$isPost	= ($_SERVER['REQUEST_METHOD'] == 'POST');
$prevId	= (isset($_GET['prev']) ? $_GET['prev'] : false);

if ($isPost) {
    try {
    	$db		 = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
        $handler = new B($db);
        
        $file   = $handler->uploadFile('file');
        $input  = $handler->processInput('info');
        
        header('Location: index.php?prev=' . $file['id']);
        
    } catch (PDOException $e) {
    	$error = 'DB error: ' . $e->getMessage();
    	
    } catch (Exception $ex) {
        $error = $ex->getMessage();
    }
}

require_once 'main.tpl';
