<?php

class A
{
    /* @var integer */
    public $publicProperty = 0;
    
    /* @var boolean */
    protected $protectedProperty = false;
    
    
    /**
     *  class constructor
     */
    public function __construct()
    {
        $this->protectedProperty = true;
    }
    
    /**
     * protected function 
     *
     * @param   stdClass|null   $var
     * @return  stdClass
     */
    protected function protectedFunction(stdClass $var = null)
    {
        return $var;
    }
    
    /**
     * private function 
     *
     * @return string
     */
    private function privateFunction()
    {
        return date('Y-m-d');
    }
    
}