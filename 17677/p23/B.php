<?php

include_once 'A.php';

class B extends A
{
    /* @var string */
    protected $uploadDir;
    
    //
    // OVERRIDED METHODS (p3)
    //

    public function __construct($dirname)
    {
        $this->uploadDir = $dirname;
    }
    
    protected function protectedFunction(stdClass $var = null)
    {
        if (empty($var)) {
            throw new Exception('Empty object!');
        }
            
        return parent::protectedFunction($var);
    }
    
    private function privateFunction()
    {
        return date('Y-m-d H:i:s');
    }
    
    
    //
    // NEW METHODS (p3)
    //
    
    public function newPublicMethod()
    {
        
    }
    
    protected function newProtectedMethod()
    {
        
    }
    
    private function newPrivateMethod()
    {
        
    }
    
    //
    // MAIN PART (p4)
    //
    
    public function processInput($name)
    {
        $value = $_REQUEST[$name];
        $value = htmlspecialchars($value);
        
        return $value;
    }
    
    public function uploadFile($name)
    {
        if (!$_FILES[$name]['name'] && !$_FILES[$name]['size']) {
            return [];
        }
        
        if ($_FILES[$name]['error'] != 0) {
            throw new Exception('Error while downloading file');
        }
        
        $uploadDir = $this->uploadDir;
        if (!file_exists($uploadDir) && !mkdir($uploadDir, 0777)) {
            throw new Exception('Cant create directory: ' . realpath($uploadDir));
        }
        
        $filepath = $uploadDir . basename($_FILES[$name]['name']);

        if (!copy($_FILES['file']['tmp_name'], $filepath)) {
            throw new Exception('Unable to create file: ' . $filepath);
        }
        
        return [
            'name'  => $_FILES[$name]['name'],
            'path'  => realpath($filepath),
        ];
    }
    
}
