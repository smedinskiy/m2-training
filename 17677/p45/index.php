<?php

include_once '../p23/B.php';

const UPLOAD_DIRECTORY = '../files/';

$isPost = ($_SERVER['REQUEST_METHOD'] == 'POST');

if ($isPost) {
    
    try {
        $handler = new B(UPLOAD_DIRECTORY);
        
        $file   = $handler->uploadFile('file');
        $input  = $handler->processInput('info');
        
    } catch (Exception $ex) {
        $error = $ex->getMessage();
    }

}

require_once 'main.tpl';
