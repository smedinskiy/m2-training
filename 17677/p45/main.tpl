<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="CyberHULL training">
	<meta name="author" content="Medinskiy">

    <title>Task #17677</title>

    <style type="text/css">
        .error {
            color: red;
            font-weight: bold;
        }
    </style>
</head>

<body>

<?php if ($isPost): ?>

<h2>Uploading result</h2>
<div>
    <?php if (!empty($error)) : ?>
        <p class="error"><?php echo $error ?></p>
    <?php else : ?>
        Input text: <i><?php echo $input ?></i><br>
        File name: <i><?php echo $file['name'] ?></i><br>
        File path: <i><?php echo $file['path'] ?></i><br>
    <?php endif; ?>
</div>

<form method="get" action="index.php">
    <input type="submit" value="Send new">
</form>

<?php else : ?>

<h2>File uploading</h2>
<form enctype="multipart/form-data" method="post" action="index.php">
    <input type="text" name="info"><br>
    <input type="file" name="file"><br>
    <input type="submit" value="Send">
</form>

<?php endif; ?>

</body>
</html>
