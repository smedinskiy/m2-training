<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="CyberHULL training">
	<meta name="author" content="Medinskiy">

    <title>Task #17677</title>

    <style type="text/css">
        table {
            border-collapse: collapse;
        }
        th {
            background: #ccc;
            text-align: left;
        }
        td, th {
            border: 1px solid #800;
            padding: 4px;
        } 
    </style>
</head>

<body>

<h2>Products info</h2>

<table>
    <tr>
        <th>ID
        <th>Name
        <th>Description
    </tr>
    <?php foreach ($products as $item): ?>
    <tr>
        <td><?php echo $item->id ?>
        <td><?php echo $item->name ?>
        <td><?php echo $item->desc ?>
    </tr>
    <?php endforeach; ?>
</table>

</body>
</html>
