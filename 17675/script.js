
$(function(){
	
	$("#selector").change(function(){
		
		var url = $(this).find(":selected").attr("path");
		
		if (url) {
			$.ajax({
				method: "GET",
				url: url
				
			}).done(function(data) {
				$("#content").html(data);
				
			}).error(function() {
				alert('Error while ajax-request!');
			});
			
		} else {
			$("#content").html("");
		}
	});
	
	$(".control").on('click', function(){
		
		$("#content").toggle();
		
		if ($(this).text() == 'hide') {
			$(this).text('show');
		} else {
			$(this).text('hide');
		}
		
	});
	
});