
$(function(){
	
	$(window).resize(function(){
 
        var width = $(window).width();
 
        if (width < 640) {
            
			$('.search-form input').css({width:'160px', 'margin-right':'20px'});
			$('.header-middle').css('padding-top', '50px');
			$('.logo-text').css('font-size', '30px');
			$('.main-menu span').css({'font-size':'10px', 'padding':'10px 10px 5px 10px'});
			$('.header-bottom').css('padding', '6px 0 6px 0');
			$('.main-menu-current').css('padding', '20px 21px 14px 21px');
			$('.banner').css('height', '290px');
			$('.feature-title').css('font-size', '16px');
			$('#products > li').css({'width':'140px', 'height':'290px'});
			$('#products .product-picture').css({'height':'140px'});
			$('#products .product-title').css({'font-size':'14px', 'padding':'10px 0 6px 0'});
			$('#products .picture-actions span').css({'font-size':'12px', 'padding':'3px'});
			$('.after-products').css('height', '30px');
			$('.page-menu span').css({'margin':'0 1px 0 1px'});
			$('.page-menu').css({'font-size':'12px'});
			$('.footer-menu').css({'font-size':'12px'});
			$('.footer-menu div').css({'margin-top':'12px'});
			$('.footer-menu-title').css({'font-size':'14px'});
			
			$('#products > li:last').hide();
 
        } else {
            
            $('.search-form input').css({width:'280px', 'margin-right':'0'});
			$('.header-middle').css('padding-top', '30px');
			$('.logo-text').css('font-size', '40px');
			$('.main-menu span').css({'font-size':'16px', 'padding':'20px 20px 15px 20px'});
			$('.header-bottom').css('padding', '11px 0 11px 0');
			$('.main-menu-current').css('padding', '20px 21px 16px 21px');
			$('.banner').css('height', '390px');
			$('.feature-title').css('font-size', '24px');
			$('#products > li').css({'width':'200px', 'height':'390px'});
			$('#products .product-picture').css({'height':'250px'});
			$('#products .product-title').css({'font-size':'20px', 'padding':'18px 0 12px 0'});
			$('#products .picture-actions span').css({'font-size':'18px', 'padding':'6px'});
			$('.after-products').css('height', '160px');
			$('.page-menu span').css({'margin':'0 4px 0 4px'});
			$('.page-menu').css({'font-size':'16px'});
			$('.footer-menu').css({'font-size':'16px'});
			$('.footer-menu div').css({'margin-top':'0px'});
			$('.footer-menu-title').css({'font-size':'18px'});
			
			$('#products > li:last').show();
        }
 
    });
	
	$(window).resize();
});
